package com.example.myfacebook

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import android.widget.EditText
import android.widget.Toast


private const val PREFS_NAME = "my_facebook_prefs"
private const val KEY_API = "my_facebook_keyApi"

class TokenActivty:AppCompatActivity() {
    private lateinit var tokenTexBox:EditText
    private lateinit var clearBtn:Button
    private lateinit var saveBtn:Button
    private lateinit var toast: Toast
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.token_layout)

        val settings = getSharedPreferences(PREFS_NAME,0)
        val apiKey = settings.getString(KEY_API,null)

        tokenTexBox=findViewById(R.id.editTextTextPersonName)
        tokenTexBox.setText(apiKey)

        clearBtn=findViewById(R.id.clearBtn)
        clearBtn.setOnClickListener{
            tokenTexBox.text.clear()
        }

        saveBtn=findViewById(R.id.saveBtn)
        saveBtn.setOnClickListener{
            val settings = getSharedPreferences(PREFS_NAME,0)
            with(settings.edit()){
                putString(KEY_API,tokenTexBox.getText().toString())
                apply()
            }
            val toast = Toast.makeText(applicationContext,"Key edited",Toast.LENGTH_LONG)
            toast.show()
            startActivity(Intent(this,MyFeed::class.java))
        }



    }
}
