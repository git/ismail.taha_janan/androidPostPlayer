package com.example.myfacebook

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.myfacebook.R.layout.my_feed_layout
import model.Post

class PostRecycleViewAdapter(private var postsList: List<Post>)
    : RecyclerView.Adapter<PostRecycleViewAdapter.PostViewHolder>()
{


    override fun getItemCount(): Int =postsList.size



    class PostViewHolder(itemView: View) :
        ViewHolder(itemView) {

        val viewName: TextView = itemView.findViewById<TextView>(R.id.postTitle)
        val viewContent: TextView = itemView.findViewById<TextView>(R.id.postContent)

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view =LayoutInflater.from(parent.context)
            .inflate(/* resource = */ my_feed_layout,/* root = */ parent,/* attachToRoot = */ false)
        return PostViewHolder(view)
    }


    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val itemsVM = postsList[position]

        holder.viewName.text=itemsVM.title
        holder.viewContent.text = itemsVM.body
    }


}