package com.example.myfacebook

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import model.Post
import serivices.ApiClient
import kotlin.math.log

class MyFeed:AppCompatActivity()
{
    private lateinit var tokenBtn:FloatingActionButton
    private var data:List<Post> = mutableListOf()
    private  lateinit var recyclerViewPost:RecyclerView




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_feed_layout)

        tokenBtn = findViewById(R.id.floatingActBtnToken)
        tokenBtn.setOnClickListener { startActivity(Intent(this, TokenActivty::class.java)) }
        executeCall()
        recyclerViewPost = findViewById(R.id.rv_posts)
        recyclerViewPost.layoutManager=LinearLayoutManager(this)
        recyclerViewPost.adapter = PostRecycleViewAdapter(data)

    }



    //@OptIn(DelicateCoroutinesApi::class)
    private fun executeCall() {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = ApiClient.postApi.getAll()

                if (response.isSuccessful && response.body() != null) {
                    val content = response.body()
                    if (content != null) {
                        print(content)
                        data = content
                        Toast.makeText(
                            this@MyFeed,
                            "Data is here",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        this@MyFeed,
                        "Error Occurred: ${response.message()}",
                        Toast.LENGTH_LONG
                    ).show()
                }

            } catch (e: Exception) {
                Toast.makeText(
                    this@MyFeed,
                    "Error Occurred: ${e.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    //override fun onPostSelected(postId: Long) {
        //listener?.onDogSelected(dogId)
        //listener?.
    //S}
}