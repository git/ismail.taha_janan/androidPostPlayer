package serivices

import model.Post
import retrofit2.Response
import retrofit2.http.GET

interface PostApiRequest {

    @GET("posts")
    suspend fun getAll(): Response<MutableList<Post>>
}